## Este projeto vem com a ideia de compartilhar um laboratório utilizando **kubernetes** e práticas de **CI\CD com gitlab**.



### Desenho da Arquitetura Proposta:
![](https://github.com/dmoraesrs/devops-cicd/blob/dev/projeto.png)

### Tecnologias utilizadas
Para este projeto serão utilizadas as tecnologias:

Cloud: [Azure](https://azure.microsoft.com/pt-br/) (para a utilização de todos os componentes a Aws não fornece creditos)

CI\CD: [Gitlab](https://gitlab.com/users/sign_in/)

Ingress Controller: [ingress-nginx](https://kubernetes.github.io/ingress-nginx/)

Cert-manager: [cert-manager](https://cert-manager.io/docs/)



